function createNewUser() {

    // adding firstname and lastname to an object 
    const newUser = {
        _firstName: prompt("Enter your name"),
        _lastName: prompt("Enter your last name"),
        get fullName() {
            return `${this._firstName} ${this._lastName}`;
        }
    };

    newUser.setFirstName = function (firstName) {
        this._firstName = firstName;
    };

    newUser.setLastName = function (lastName) {
        this._lastName = lastName;
    };

    //   adding getLogin function 
    newUser.getLogin = function () {
        return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
    };

    newUser.birthday = prompt("Enter your birthday in dd.mm.yyyy");

    // adding getAge function
    newUser.getAge = function () {

        // changing input format to prompt native mm.dd.yyyy
        let slicedBirthday = newUser.birthday.slice(3, 6);
        const slicedYear = newUser.birthday.slice(6);
        // transfering slicedYear to newUser object
        newUser.birthYear = slicedYear;
        let correctedBirthday = slicedBirthday + newUser.birthday[0] + newUser.birthday[1] +
            newUser.birthday[2] + slicedYear;

        // forming date object from native 
        let convertCorrectedBdayToDate = new Date(correctedBirthday);

        // getting user birthday and todays date in milliseconds
        let birthdayInMs = convertCorrectedBdayToDate.getTime();
        let todayDate = Date.now();
        const ageInMs = todayDate - birthdayInMs;

        // defining how old are you
        const years = Math.floor(ageInMs / (1000 * 60 * 60 * 24 * 365.25));
        const months = Math.floor((ageInMs % (1000 * 60 * 60 * 24 * 365.25)) / (1000 * 60 * 60 * 24 * (365.25 / 12)));
        const days = new Date().getDate();

        console.log("Your birthday is: " + newUser.birthday);

        return console.log("Your age: " + years + " year(s) " + months + " month(s) " + days + " day(s)");
    };

    newUser.getPassword = function () {
        return this._firstName[0].toUpperCase() + this._lastName.toLowerCase();
    };

    return newUser;
}

const user = createNewUser();

console.log("You entered: " + user.fullName);
console.log("Your suggested login is: " + user.getLogin());
user.getAge();
console.log("Your suggested password is: " + user.getPassword() + user.birthYear);