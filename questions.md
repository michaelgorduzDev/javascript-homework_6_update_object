1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Escaping using mostly in Regular Expressions (RegEx), to let the IDE know that expression 
after special character is not a code. In JavaScript that symbol is BACKSLASH (\).


2. Які засоби оголошення функцій ви знаєте?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
a) Function declaration - is a clissic way to declare a function in JavaScript. Example:
function doSomething() { 
    your code here;
    };

b) Function expression (Anonymous function) - in this case you assigning the function to a variable. Example:
let doSomething = function() {
    your code here;
};

c)  Named function expression - almost the same as previous, but this fuction has a name. Example:
let doSomething = function foo() {
    your code here;
};

3. Що таке hoisting, як він працює для змінних та функцій?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In JavaScript Hoisting allows you to use functions and variables before they are declared. That fact causes
a many problems and bugs, so you have to avoid Hoisting
